FROM adoptopenjdk/openjdk11:alpine-jre

WORKDIR /app

COPY target/transportePublico-0.0.1-SNAPSHOT.jar /app/transportePublico.jar

COPY PontosTaxi.txt /app/PontosTaxi.txt

ENTRYPOINT ["java", "-jar", "transportePublico.jar"] 