package com.transporte.transportePublico.model;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class Onibus{

	@Id
	private String id;
	private String codigo;
	private String nome;

	
}
