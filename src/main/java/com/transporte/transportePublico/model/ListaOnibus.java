package com.transporte.transportePublico.model;

import java.util.*;
import lombok.Data;

@Data
public class ListaOnibus {

	private List<Onibus> listaOnibus = new ArrayList<Onibus>();
}
