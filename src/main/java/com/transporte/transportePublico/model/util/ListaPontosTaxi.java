package com.transporte.transportePublico.model.util;

import java.io.IOException;
import java.util.*;
import java.text.*;

import com.transporte.transportePublico.model.PontoTaxi;

public class ListaPontosTaxi {
	
	public static List<PontoTaxi> lista;
	
	public static void listarPontosTaxi(){
		String path = "PontosTaxi.txt";
		List<String> listaPontos;
		List<PontoTaxi> retorno = new ArrayList<PontoTaxi>();
		try {
			listaPontos = ManipuladorArquivo.leitor(path);
			for (String pontoString : listaPontos) {
				if (pontoString != null) {
					String[] textoSeparado = pontoString.split("#");
					
					PontoTaxi ponto = new PontoTaxi();
					ponto.setNomePonto(textoSeparado[0]);
					ponto.setLatitude(textoSeparado[1]);
					ponto.setLongitude(textoSeparado[2]);
					
					DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSS");
					Date date = (Date)formatter.parse(textoSeparado[3]);
					
					ponto.setDataCadastro(date);
					
					retorno.add(ponto);
					
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		lista = retorno;
	}
}

