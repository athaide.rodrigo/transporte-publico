package com.transporte.transportePublico.model.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.File;
import java.util.*;

public class ManipuladorArquivo {

	public static List<String> leitor(String path) throws IOException {
		List<String> retorno = new ArrayList<String>();
		BufferedReader buffRead = new BufferedReader(new FileReader(path));
		String linha = "";
		while (true) {
			if (linha == null) {
				break;
			}
			linha = buffRead.readLine();
			retorno.add(linha);
		}
		buffRead.close();
		return retorno;
	}

	public static void escrever(String path, String pontoTaxi) {
		File arquivo = new File(path);
		try {
			if (!arquivo.exists()) {
				// cria um arquivo (vazio)
				arquivo.createNewFile();
			}
			FileWriter fw = new FileWriter(arquivo, true);
			
			// escreve no arquivo
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(pontoTaxi);
			bw.newLine();
			bw.close();
			fw.close();

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

}
