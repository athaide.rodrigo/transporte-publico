package com.transporte.transportePublico.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;

import lombok.Data;

@Data
@Entity
public class Intinerario {
	
	  @Id
	  private String idlinha;
	  private String nome;
	  private String codigo;
	  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	  @JoinTable(name="INTINERARIO_LATLNG",
	             joinColumns={@JoinColumn(name = "IDLINHA")},
	             inverseJoinColumns={@JoinColumn(name = "IDLATLNG")})
	  List<LatLng> coordenadas = new ArrayList<LatLng>();

}
