package com.transporte.transportePublico.model;

import java.util.*;
import java.text.*;

import lombok.Data;

@Data
public class PontoTaxi {

	private String nomePonto;
	private String latitude;
	private String longitude;
	private Date dataCadastro;
	
	@Override
	public String toString() {
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
		String retorno =  nomePonto + "#" + latitude + "#" + longitude + "#" + sdf.format(dataCadastro);
		return retorno;
	}
}
