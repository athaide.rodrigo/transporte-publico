package com.transporte.transportePublico.service;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import com.google.gson.Gson;
import com.transporte.transportePublico.model.Intinerario;
import com.transporte.transportePublico.model.ListaOnibus;
import com.transporte.transportePublico.model.Onibus;
import com.transporte.transportePublico.model.PontoTaxi;
import com.transporte.transportePublico.model.util.ListaPontosTaxi;
import com.transporte.transportePublico.model.util.ManipuladorArquivo;
import com.transporte.transportePublico.repository.IntinerarioRepository;
import com.transporte.transportePublico.repository.OnibusRepository;

import reactor.core.publisher.Mono;

@Service
public class TrasnportePublicoService {

	@Autowired
	private WebClient webClient;

	@Autowired
	private OnibusRepository onibusRepository;

	@Autowired
	private IntinerarioRepository intinerarioRepository;

	private Gson gson = new Gson();
	private String path = "PontosTaxi.txt";

	public ListaOnibus listarLinhasOnibus() {

		Mono<String> monoListaOnibus = this.webClient.method(HttpMethod.GET).uri("?a=nc&p=%&t=o")
				.accept(MediaType.TEXT_HTML).retrieve().bodyToMono(String.class);

		String retornoMono = monoListaOnibus.block();
		String objeto = "{\"listaOnibus\":" + retornoMono + "}";

		ListaOnibus listaOnibusObject = this.gson.fromJson(objeto, ListaOnibus.class);

		return listaOnibusObject;
	}

	public Intinerario listarIntinerarioUnidadeTransporte(int id) {

		String original = "";
		Intinerario intinerarioObject = new Intinerario();
		try {
			Mono<String> monoListaOnibus = this.webClient.method(HttpMethod.GET).uri("?a=il&p={id}", id)
					.accept(MediaType.TEXT_HTML).retrieve().bodyToMono(String.class);

			String retornoMono = monoListaOnibus.block();
			original = new String(retornoMono);

			retornoMono = retornoMono.replace("\"" + 0 + "\":", "\"coordenadas\":[ ");
			int i = 1;
			while (retornoMono.contains("\"" + i + "\":")) {
				retornoMono = retornoMono.replace("\"" + i + "\":{", "{");
				i++;
			}
			retornoMono = retornoMono.replace("}}", "}]}");

			intinerarioObject = gson.fromJson(retornoMono, Intinerario.class);

		} catch (Exception e) {
			System.out.println("erro encontrado neste:");
			System.out.println(e.getMessage());
			System.out.println(original);
		}

		return intinerarioObject;
	}

	public PontoTaxi cadastrarPontoTaxi(PontoTaxi pontoTaxi) throws IOException {

		List<PontoTaxi> pontosCadastrados = ListaPontosTaxi.lista;
		
		if(!pontosCadastrados.contains(pontoTaxi)) {
			ManipuladorArquivo.escrever(path, pontoTaxi.toString());
			ListaPontosTaxi.lista.add(pontoTaxi);
		} 
		
		return pontoTaxi;
	}

	public Onibus salvarOnibus(Onibus onibus) {
		if(!onibusRepository.existsById(onibus.getId())) {
			return onibusRepository.save(onibus);
		} else {
			return null;
		}

	}

	public List<Onibus> listarOnibus() {
		return onibusRepository.findAll();

	}
	
	public Intinerario salvarIntinerarios(Intinerario intinerario) {
		List<Intinerario> listaIntinerarios = listarIntinerarios();
		
		if(!listaIntinerarios.contains(intinerario)) {
			return intinerarioRepository.save(intinerario);
		} else {
			return null;
		}
	}
	
	public List<Intinerario> listarIntinerarios() {
		return intinerarioRepository.findAll();
	}

}
