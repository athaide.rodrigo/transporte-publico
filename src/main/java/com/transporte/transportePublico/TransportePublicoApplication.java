package com.transporte.transportePublico;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;

import com.transporte.transportePublico.model.util.ListaPontosTaxi;

@SpringBootApplication
public class TransportePublicoApplication {

	@Bean
	public WebClient webClient(WebClient.Builder builder) {
		
		return builder
				.baseUrl("http://www.poatransporte.com.br/php/facades/process.php")
				.defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_HTML_VALUE)
				.build();
	}
	
	public static void main(String[] args) {
		ListaPontosTaxi.listarPontosTaxi();
		SpringApplication.run(TransportePublicoApplication.class, args);
		
	}
}
