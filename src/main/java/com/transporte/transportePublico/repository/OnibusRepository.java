package com.transporte.transportePublico.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.transporte.transportePublico.model.Onibus;

@Repository
public interface OnibusRepository extends JpaRepository<Onibus, String>{

}
