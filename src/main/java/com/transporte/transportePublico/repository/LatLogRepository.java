package com.transporte.transportePublico.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.transporte.transportePublico.model.LatLng;

@Repository
public interface LatLogRepository extends JpaRepository<LatLng, Long>{

}
