package com.transporte.transportePublico.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.transporte.transportePublico.model.Intinerario;

@Repository
public interface IntinerarioRepository extends JpaRepository<Intinerario, String>{

}
