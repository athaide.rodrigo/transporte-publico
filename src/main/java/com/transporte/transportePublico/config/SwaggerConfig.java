package com.transporte.transportePublico.config;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.annotations.ApiIgnore;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
	
	@Value("${portal.swagger.path}")
	private String swaggerPath;

	@Bean
	public Docket allApi() {
//		return new Docket(DocumentationType.SWAGGER_2).host(swaggerPath)
//		.select()
//        .apis(RequestHandlerSelectors.any())
//        .paths(PathSelectors.any())
//        .build();
		
		Set<String> protocols = new HashSet<>();
		protocols.add("http");
		protocols.add("https");
		
		return new Docket(DocumentationType.SWAGGER_2).host(swaggerPath)
				  .groupName("All")
				  .apiInfo(apiInfo())
				  .select()
		          .apis(RequestHandlerSelectors.basePackage("com.transporte.transportePublico.controller"))
				  .paths(PathSelectors.any())
				  .build()
				  .protocols(protocols)
				  .ignoredParameterTypes(ApiIgnore.class)
				  .enableUrlTemplating(true);
				  
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder()
				.title("Transportes Publicos")
				.description("Serviços da api de transportes publicos")
				.termsOfServiceUrl("http://localhost:8080")
				.license("")
				.licenseUrl("http://localhost:8080")
				.version("1.0")
				.build();
	}
}
