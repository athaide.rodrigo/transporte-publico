package com.transporte.transportePublico.controller;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.transporte.transportePublico.model.GeoCoordinate;
import com.transporte.transportePublico.model.Intinerario;
import com.transporte.transportePublico.model.LatLng;
import com.transporte.transportePublico.model.ListaOnibus;
import com.transporte.transportePublico.model.Onibus;
import com.transporte.transportePublico.model.PontoTaxi;
import com.transporte.transportePublico.model.util.GeoUtils;
import com.transporte.transportePublico.model.util.ListaPontosTaxi;
import com.transporte.transportePublico.service.TrasnportePublicoService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/transportePublico")
public class TransportePublicoController {

	@Autowired
	private TrasnportePublicoService trasnportePublicoService;

	@ApiOperation(value = "Retorna uma lista de Linhas de ônibus filtrada pelo nome")
	@GetMapping("/listarPorNome/{nome}")
	public ListaOnibus listarPorNome(@RequestParam("nome") String nome) {
		ListaOnibus listaRetorno = new ListaOnibus();
		listaRetorno.setListaOnibus(new ArrayList<Onibus>());
		ListaOnibus lista = trasnportePublicoService.listarLinhasOnibus();
		for (Onibus o : lista.getListaOnibus()) {
			if (o.getNome().equalsIgnoreCase(nome)) {
				listaRetorno.getListaOnibus().add(o);
			}
		}
		return listaRetorno;
//		return null;
	}
	
	@ApiOperation(value = "Retorna uma lista de Linhas de ônibus oriunda do portal datapoa")
	@GetMapping
	public ListaOnibus listarTodos() {

		return trasnportePublicoService.listarLinhasOnibus();
	}

	@ApiOperation(value = "Retorna uma lista de Intinerarios de ônibus oriunda do portal datapoa")
	@GetMapping("/{id}")
	public Intinerario listarIntinerarioUnidadeTransporte(@PathVariable int id) {

		return trasnportePublicoService.listarIntinerarioUnidadeTransporte(id);
	}

	@ApiOperation(value = "Retorna uma lista de Linhas de onibus que passam em um determinado raio de abrangencia")
	@GetMapping("/listarIntinerarios/{lat}/{lng}/{raio}")
	public ListaOnibus linhasEmUmRaio(@RequestParam("lat") double latitude, @RequestParam("lng") double longitude,
			@RequestParam("raio") double raio) {
		// Criar a lista com todos os Onibus
		ListaOnibus lista = trasnportePublicoService.listarLinhasOnibus();
		ListaOnibus listaRetorno = new ListaOnibus();
		// Criar lista de intinerarios
		ArrayList<Intinerario> listaIntinerarios = new ArrayList<Intinerario>();
		System.out.println("lista de onibus: " + lista.getListaOnibus().size());
		for (Onibus onibus : lista.getListaOnibus()) {
			Integer id = Integer.parseInt(onibus.getId());
			listaIntinerarios.add(trasnportePublicoService.listarIntinerarioUnidadeTransporte(id));
		}

		GeoCoordinate first = new GeoCoordinate(latitude, longitude);

		for (Intinerario intinerario : listaIntinerarios) {
			boolean isNoIntinerario = false;
			for (LatLng latlog : intinerario.getCoordenadas()) {
				GeoCoordinate second = new GeoCoordinate(latlog.getLat(), latlog.getLng());

				double quantosKM = GeoUtils.geoDistanceInKm(first, second);

				if (quantosKM < raio) {
					isNoIntinerario = true;
				}
			}
			if (isNoIntinerario) {

				Onibus onibus = new Onibus();
				onibus.setId(intinerario.getIdlinha().toString());
				onibus.setCodigo(intinerario.getCodigo());
				onibus.setNome(intinerario.getNome());
				listaRetorno.getListaOnibus().add(onibus);
			}
		}

		System.out.println("qtd de linhas no raio: " + listaRetorno.getListaOnibus().size());

		return listaRetorno;
	}

	@ApiOperation(value = "Cadastra ponto de taxi")
	@PostMapping
	public PontoTaxi cadastrar(@RequestBody PontoTaxi pontoTaxi) throws Exception {
		pontoTaxi.setDataCadastro(new Date());
		return this.trasnportePublicoService.cadastrarPontoTaxi(pontoTaxi);
		
	}

	@ApiOperation(value = "Retorna os pontos de taxi")
	@GetMapping("/retornarPontosTaxi")
	public List<PontoTaxi> retornarPontosTaxi() {
		return ListaPontosTaxi.lista;
	}
	
	@ApiOperation(value = "Cadastrar novas Linhas de onibus")
	@PostMapping("/cadastrarLinhas")
	public ResponseEntity<Onibus> cadastrarLinhas(@RequestBody Onibus onibus) {

		Onibus retorno = this.trasnportePublicoService.salvarOnibus(onibus);
		
		if (retorno == null) {
			return ResponseEntity.badRequest().build();
		} else {
			return ResponseEntity.ok(retorno);
		}
	}
	
	@ApiOperation(value = "Retorna uma lista de Linhas de onibus cadastradas")
	@GetMapping("/pesquisarLinhas")
	public List<Onibus> pesquisarLinhas() {
		return this.trasnportePublicoService.listarOnibus();
	}
	
	@ApiOperation(value = "Cadastar intinerarios de determinada linha de onibus")
	@PostMapping("/cadastrarIntinerarios")
	public ResponseEntity<Intinerario> cadastrarIntinerarios(@RequestBody Intinerario intinerario) {
		
		Intinerario retorno = this.trasnportePublicoService.salvarIntinerarios(intinerario);
		
		if (retorno == null) {
			return ResponseEntity.badRequest().build();
		} else {
			return ResponseEntity.ok(retorno);
		}
	}
	
	@ApiOperation(value = "Listar intinerarios cadastrados")
	@GetMapping("/pesquisarIntinerarios")
	public List<Intinerario> pesquisarIntinerarios() {

		return this.trasnportePublicoService.listarIntinerarios();
	}

}
